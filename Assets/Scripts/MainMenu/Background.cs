﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

	private KeepUpdated keep_updated;

	void Start() {
		MainCamera.keep_updated.Register(GameObject.Find("Background"), KeepUpdated.AspectMode.Scale);
	}

	// Update is called once per frame
	void Update () {
	}
}
