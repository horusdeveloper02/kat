using UnityEngine;
using System.Collections;
using System;

public class KeepUpdated{

	private float old_width;
	private float old_height;
	private ArrayList data;
	private Camera cam;
	
	public enum AspectMode{
		Scale,Propotional,CallbackOnly
	}

	public KeepUpdated(Camera cam){
		data=new ArrayList();
		this.cam=cam;
		old_width=Screen.width;
		old_height=Screen.height;
		cam.orthographicSize= Screen.height/2;
	}
	
	public void Register(GameObject obj, AspectMode type, Action<GameObject,float,float> callback, Vector2 middle_point){
		float width=obj.transform.localScale.x;
		float height=obj.transform.localScale.z;
		float original_x=obj.transform.position.x;
		float original_y=obj.transform.position.z;
		ComponentData comp_data=new ComponentData(obj, original_x, original_y, width, height, type, callback,middle_point);
		cam.orthographicSize= Screen.height/2;
		updateCompData(comp_data);
		data.Add(comp_data);
	}

	public void Register(GameObject obj, AspectMode type, Action<GameObject,float,float> callback){
		Register(obj,type,callback,new Vector2(0,0));
	}

	public void Register(GameObject obj, AspectMode type, Vector2 middle_point){
		Register(obj,type,null,middle_point);
	}

	public void Register(GameObject obj, AspectMode type){
		Register(obj,type,null,new Vector2(0,0));
	}

	private bool updateCompData(ComponentData comp_data){
		if (comp_data==null || comp_data.obj==null) {
			return false;
		} else {
			if (comp_data.mode==AspectMode.Scale){
				UpdateScale(comp_data.obj,comp_data.original_x, comp_data.original_y,comp_data.width,comp_data.height, comp_data.callback, comp_data.middle_point);
			} else if (comp_data.mode==AspectMode.Propotional){
				UpdateAspectRatio(comp_data.obj,comp_data.original_x, comp_data.original_y,comp_data.width,comp_data.height, comp_data.callback, comp_data.middle_point);
			} else if (comp_data.mode==AspectMode.CallbackOnly){
				CallbackOnly(comp_data.obj, comp_data.original_x, comp_data.original_y, comp_data.callback);
			}
			return true;
		}
	}

	// Scales the element without keeping aspect ratio
	private void UpdateScale (GameObject obj, float original_x, float original_y, float width, float height, Action<GameObject,float,float> callback, Vector2 middle_point) {
		obj.transform.localScale = new Vector3(DefaultScreen.adjustWidth(width), 1, DefaultScreen.adjustHeight(height));
		DefaultScreen.updatePosition(obj, original_x,original_y, middle_point);
		if (callback!=null) {
			callback(obj,original_x,original_y);
		}
	}

	// Scales the element keeping it's aspect ratio. This only updates when the height changes
	private void UpdateAspectRatio(GameObject obj, float original_x, float original_y, float width, float height, Action<GameObject,float,float> callback, Vector2 middle_point) {
		if (Screen.height>Screen.width) {
			float aspect_ratio=width/height;
			float new_height=DefaultScreen.adjustHeight(height);
			float new_width=new_height*aspect_ratio;
			obj.transform.localScale = new Vector3(new_width, 1, new_height);
			DefaultScreen.updatePosition(obj, original_x,original_y, middle_point);
			if (callback!=null) {
				callback(obj,original_x,original_y);
			}
		} else if (Screen.width>Screen.height){
			float aspect_ratio=height/width;
			float new_width=DefaultScreen.adjustWidth(width);
			float new_height=new_width*aspect_ratio;
			obj.transform.localScale = new Vector3(new_width, 1, new_height);
			DefaultScreen.updatePosition(obj, original_x,original_y, middle_point);
			if (callback!=null) {
				callback(obj,original_x,original_y);
			}
		}
	}

	// Used to configure the element position and other things when the Screen Resolution is updated.
	private void CallbackOnly (GameObject obj, float original_x, float original_y, Action<GameObject,float,float> callback) {
		callback(obj,original_x,original_y);
	}

	// public void UpdateAspectRatio(float width, float height) {
	// 	UpdateAspectRatio(width,height,null);
	// }

	private class ComponentData {
		public GameObject obj;
		public float original_x;
		public float original_y;
		public float width;
		public float height;
		public AspectMode mode;
		public Action<GameObject,float,float> callback;
		public Vector2 middle_point;

		public ComponentData(GameObject obj, float original_x, float original_y, float width, float height, AspectMode mode, Action<GameObject,float,float> callback, Vector2 middle_point){
			this.obj=obj;
			this.original_x=original_x;
			this.original_y=original_y;
			this.width=width;
			this.height=height;
			this.mode=mode;
			this.middle_point=middle_point;
			this.callback=callback;
		}
	}
}