using UnityEngine;
using System.Collections;

public class DefaultScreen {

	public static float DEFAULT_WIDTH=640;
	public static float DEFAULT_HEIGHT=960;

	public enum Position{
		Portrait, LandScape
	}

	public static void definePosition(DefaultScreen.Position pos){
		if (pos==DefaultScreen.Position.Portrait){
			DEFAULT_WIDTH=640;
			DEFAULT_HEIGHT=960;
		} else if (pos==DefaultScreen.Position.LandScape){
			DEFAULT_HEIGHT=640;
			DEFAULT_WIDTH=960;
		}
	}

	public static float adjustWidth(float width){
		return Screen.width / DEFAULT_WIDTH * width;
	}

	public static float adjustHeight(float height){
		return Screen.height / DEFAULT_HEIGHT * height;
	}

	public static Vector2 calculatePosition(float x, float y, Vector2 middle_point){
		// float left=(middle_point.x-DEFAULT_WIDTH/2);
		// float bottom=(middle_point.y-DEFAULT_HEIGHT/2);
		// float dif_x=left-x;
		// float dif_y=bottom-y;
		// float new_x=adjustWidth(dif_x);
		// if (x<middle_point.x)
		// 	new_x=left+new_x;
		// else
		// 	new_x=DEFAULT_WIDTH-new_x;
		// float new_y=adjustHeight(dif_y);
		// if (y<middle_point.y)
		// 	new_y=bottom+new_y;
		// else
		// 	new_y=DEFAULT_HEIGHT-new_y;
		// return new Vector2(new_x,new_y);
		return new Vector2(adjustWidth(x),adjustHeight(y));
	}

	public static Vector2 calculatePosition(float x, float y){
		return calculatePosition(x, y, new Vector2(0,0));
	}

	public static Vector3 calculatePosition(float x, float y, float z, Vector2 middle_point){
		Vector2 temp=calculatePosition(x, z, middle_point);
		return new Vector3(temp.x,y,temp.y);
	}

	public static Vector3 calculatePosition(float x, float y, float z){
		return calculatePosition(x, y, z, new Vector2(0,0));
	}

	public static void updatePosition(GameObject obj, float x, float y, Vector2 middle_point){
		Vector2 calculated=calculatePosition(x,y,middle_point);
		obj.transform.position=new Vector3(calculated.x, obj.transform.position.y, calculated.y);
	}

	public static void updatePosition(GameObject obj, float x, float y){
		Vector2 calculated=calculatePosition(x,y);
		obj.transform.position=new Vector3(calculated.x, obj.transform.position.y, calculated.y);
	}
}
