﻿using UnityEngine;
using System.Collections;

public class Utilities {

	public static void hideGroupObjects(GameObject group){
		foreach (Transform child in group.transform) {
			if (child.renderer!=null)
		    	child.renderer.enabled = false;
		    Utilities.hideGroupObjects(child.gameObject);
		}
	}

	public static void showGroupObjects(GameObject group){
		foreach (Transform child in group.transform) {
			if (child.renderer!=null)
		    	child.renderer.enabled = true;
		    Utilities.showGroupObjects(child.gameObject);
		}
	}

}
