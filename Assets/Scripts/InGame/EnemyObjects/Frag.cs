using UnityEngine;
using System.Collections;

public class Frag : MonoBehaviour {

	private Vector3 original_target;
	private int speed;
	public Vector3 target;
	public bool isStarted=false;

	/*
		Sets the target of the frag in order to move the target to that point with a specified speed.
	*/
	public void SetTarget(Vector3 target, int speed){
		original_target=target;
		this.target=target;
		this.speed=speed;
	}

	public void InitMovement(){
		isStarted=true;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (this.target!=null && isStarted){
			float step = speed * Time.deltaTime;
        	transform.position = Vector3.MoveTowards(transform.position, target, step);
        	// If it is out of the screen, destroy the frag
        	if (transform.position.x<=-Screen.width/2-transform.localScale.x/2 || transform.position.x>=Screen.width/2+transform.localScale.x/2 || transform.position.z<=-Screen.height/2-transform.localScale.z/2 || transform.position.z>=Screen.height/2+transform.localScale.z/2){
        		Destroy(gameObject);
        	}
		}
	}
}
