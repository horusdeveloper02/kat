using UnityEngine;
using System.Collections;

public class MCamera : MonoBehaviour {

	private float old_height;
	public static KeepUpdated keep_updated;

	void Awake() {
		keep_updated=new KeepUpdated(Camera.mainCamera);
		DefaultScreen.definePosition(DefaultScreen.Position.LandScape);
	}

	void Start() {
		keep_updated.Register(GameObject.Find("BGround"), KeepUpdated.AspectMode.Scale);
		GameObject numpad=GameObject.Find("Numpad");
		// keep_updated.Register(numpad, KeepUpdated.AspectMode.Scale, new Vector2(317,numpad.transform.position.z));
		keep_updated.Register(numpad, KeepUpdated.AspectMode.Scale);
	}

	// Update is called once per frame
	void Update () {

	}
}
