using UnityEngine;
using System.Collections;

public class MainGame2 : MonoBehaviour {

	private ArrayList bombs;
	private ArrayList frags;
	private ArrayList splits;
	private static float time_counter;
	private int number;
	private int total_blocks;
	private int result;
	private int max_blocks=5;
	private bool missile_moving=false;
	private bool frags_moving=false;
	private GameObject missile_obj;
	private Missile missile_script;
	private GameObject kounter_obj;
	public int delay=3;//Change on Unity only
	public static GameObject numpad_input_text;
	public int block_dim=80;
	public int frag_dim=30;
	public int block_gap=20;
	public int frag_speed=200;
	public int missile_speed=50;
	public static int value=0;

	// Use this for initialization
	void Start () {
		bombs=new ArrayList();
		frags=new ArrayList();
		splits=new ArrayList();
		time_counter=delay;
		DestroyBombs();
		CreateBombs();
		missile_obj=GameObject.Find("Missile");
		missile_obj.renderer.enabled=false;
		missile_script = (Missile) missile_obj.GetComponent(typeof(Missile));
		MCamera.keep_updated.Register(missile_obj, KeepUpdated.AspectMode.Propotional,delegate(GameObject obj, float orig_x, float orig_y){
			missile_script.init_pos=DefaultScreen.calculatePosition(400, 3, 0);
			missile_script.end_pos=DefaultScreen.calculatePosition(250, 3, 0);
		});
		kounter_obj=GameObject.Find("Kounter");
		numpad_input_text=GameObject.Find("Numpad/NumInput/Text");
		MCamera.keep_updated.Register(kounter_obj, KeepUpdated.AspectMode.Propotional);
	}
	
	// Update is called once per frame
	void Update () {
		time_counter -= Time.deltaTime;
		if (time_counter < 0){
			ClearRemovedFrags();
			ClearRemovedSplit2s();
			if (!missile_moving && !frags_moving) {
				if (value>0){
					missile_script.RestartMovement(missile_speed);
					missile_moving=true;
				} else {
					missile_moving=true;
				}
				//Hide the numpad
				Utilities.hideGroupObjects(GameObject.Find("Numpad"));
			} else if ((missile_moving && missile_script.HasReachedEnd()) || (value==0 && !frags_moving)){
				missile_moving=false;
				BombsToFrags();
				InputToSplit2s();
				frags_moving=true;
				StartMoveFragsAndSplit2s();
			} else if (!missile_moving && frags.Count==0 && splits.Count==0){
				DestroyBombs();
				CreateBombs();
			    time_counter=delay;
			 	kounter_obj.transform.position=new Vector3(DefaultScreen.adjustWidth(400),5,0);
				frags_moving=false;
			 	//Show the numpad
				Utilities.showGroupObjects(GameObject.Find("Numpad"));
				TextMesh numpad_t_mesh=numpad_input_text.GetComponent<TextMesh>();
		 		numpad_t_mesh.text="";
			}					
		}
	}

	public static void LaunchMissiles(string value){
		time_counter=0;
		if (value!=null && value!="")
			MainGame.value=int.Parse(value);
		else
			MainGame.value=0;
	}

	private void ClearRemovedFrags(){
		ArrayList to_remove=new ArrayList();
		foreach (Frag the_frag in frags) {
			if (the_frag==null) {
				to_remove.Add(the_frag);
			}
		}
		foreach (Frag removing in to_remove) {
			frags.Remove(removing);
		}
	}


	private void ClearRemovedSplit2s(){
		ArrayList to_remove=new ArrayList();
		foreach (Split2 the_split in splits) {
			if (the_split==null) {
				to_remove.Add(the_split);
			}
		}
		foreach (Split2 removing in to_remove) {
			splits.Remove(removing);
		}
	}

	// Create new bombs
	private void CreateBombs(){
		// 1st inclusive, 2nd exclusive when used with integers
		number=Random.Range(2, 9);
		total_blocks=Random.Range(2, 15);
		result=number*total_blocks;
		float calculated_block_dim=block_dim;
		float x_margin=block_gap;
		float y_margin=block_gap;
		float pos_z=0;
		int blocks_left_col=0;
		float pos_x=-DefaultScreen.DEFAULT_WIDTH/2+calculated_block_dim/2+x_margin-calculated_block_dim-x_margin;
		for (int i=0;i<total_blocks;i++) {
			if (blocks_left_col==0){	
				int num_blocks=total_blocks-i;
				if (num_blocks>max_blocks){
					num_blocks=max_blocks;
				}			
				float space_occupied=num_blocks*calculated_block_dim+(num_blocks-1)*y_margin;
				pos_z=space_occupied/2-calculated_block_dim/2;
				blocks_left_col=num_blocks;
				pos_x+=calculated_block_dim+x_margin;
			}
			GameObject instance = Instantiate(Resources.Load("Prefabs/EnemyBlock", typeof(GameObject))) as GameObject;
			GameObject text_obj=instance.transform.Find("Text").gameObject;
		 	TextMesh text_obj_mesh=text_obj.GetComponent<TextMesh>();
		 	text_obj_mesh.text=number.ToString();
			instance.transform.localScale=new Vector3(block_dim,2,block_dim);
			instance.transform.position=new Vector3(pos_x,2,pos_z);
			MCamera.keep_updated.Register(instance, KeepUpdated.AspectMode.Propotional);
			bombs.Add(instance);
			pos_z-=calculated_block_dim+y_margin;
			blocks_left_col--;
		}
	}

	private void BombsToFrags(){
		foreach(GameObject bomb in bombs){
			float target_gap=DefaultScreen.DEFAULT_HEIGHT/(number+1);
			float start_y=DefaultScreen.DEFAULT_HEIGHT/2-target_gap;
			for (int i=1;i<=number; i++) {
				GameObject instance = Instantiate(Resources.Load("Prefabs/Frag", typeof(GameObject))) as GameObject;
				instance.transform.localScale=new Vector3(frag_dim,1,frag_dim);
				instance.transform.position=new Vector3(bomb.transform.position.x,1,bomb.transform.position.z);
				Frag frag_script = (Frag) instance.GetComponent(typeof(Frag));
				frag_script.SetTarget(DefaultScreen.calculatePosition(500,1,start_y),frag_speed);
				MCamera.keep_updated.Register(instance, KeepUpdated.AspectMode.Propotional,delegate(GameObject obj, float orig_x, float orig_y){
					Frag the_frag = (Frag) obj.GetComponent(typeof(Frag));
					// the_frag.RefreshPosition();
				});
				frags.Add(frag_script);
				start_y-=target_gap;
			}
			Destroy(bomb);
		}
		bombs.Clear();
	}

	private void InputToSplit2s(){
		if (value>0){
			int total_splits=value;
			int total_frags=frags.Count;
			int targetted=total_splits;
			float target_gap;
			float start_y;
			if (total_splits>total_frags){
				int left_splits=total_splits-total_frags;
				target_gap=DefaultScreen.DEFAULT_HEIGHT/(left_splits+1)/2;
				start_y=DefaultScreen.DEFAULT_HEIGHT/4+target_gap;
				for (int i=1;i<=left_splits; i++) {
					GameObject instance = Instantiate(Resources.Load("Prefabs/Split2", typeof(GameObject))) as GameObject;
					instance.transform.localScale=new Vector3(frag_dim,1,frag_dim);
					instance.transform.position=new Vector3(missile_obj.transform.position.x,1,missile_obj.transform.position.z);
					Split2 split_script = (Split2) instance.GetComponent(typeof(Split2));
					split_script.SetNoFrag(DefaultScreen.calculatePosition(0,3,start_y),frag_speed);
					MCamera.keep_updated.Register(instance, KeepUpdated.AspectMode.Propotional,delegate(GameObject obj, float orig_x, float orig_y){
						Split2 the_split = (Split2) obj.GetComponent(typeof(Split2));
						the_split.RefreshPosition();
					});
					splits.Add(split_script);
					start_y-=target_gap;
				}
				targetted-=left_splits;
			}
			target_gap=DefaultScreen.DEFAULT_HEIGHT/(targetted+1);
			start_y=DefaultScreen.DEFAULT_HEIGHT/2-target_gap;
			for (int i=0;i<targetted; i++) {
				GameObject instance = Instantiate(Resources.Load("Prefabs/Split2", typeof(GameObject))) as GameObject;
				instance.transform.localScale=new Vector3(frag_dim,1,frag_dim);
				instance.transform.position=new Vector3(missile_obj.transform.position.x,1,missile_obj.transform.position.z);
				Split2 split_script = (Split2) instance.GetComponent(typeof(Split2));
				split_script.SetFrag((Frag)frags[i],frag_speed);
				MCamera.keep_updated.Register(instance, KeepUpdated.AspectMode.Propotional,delegate(GameObject obj, float orig_x, float orig_y){
					Split2 the_split = (Split2) obj.GetComponent(typeof(Split2));
			 		the_split.RefreshPosition();
				});
				splits.Add(split_script);
				start_y-=target_gap;
			}
		}
	}

	private void StartMoveFragsAndSplit2s(){
		foreach (Frag the_frag in frags) {
			the_frag.InitMovement();
		}
		foreach (Split2 the_split in splits) {
			the_split.InitMovement();
		}
	}

	private void DestroyBombs(){
		foreach (GameObject the_bomb in bombs){
			Destroy(the_bomb);
		}
		bombs.Clear();
	}
}
