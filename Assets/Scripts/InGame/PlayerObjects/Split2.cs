﻿using UnityEngine;
using System.Collections;

public class Split2 : MonoBehaviour {

	private Vector3 original_frag;
	private Vector3 init_pos;
	private float speed;
	private float speed_x;
	private float speed_y;
	private float time_passed=0;
	private float gravity=80f;
	public Frag frag;
	public Vector3 target_no_frag;
	public bool isStarted=false;

	/*
		Sets the target frag of the split in order to move the split against it with a specified speed_x.
	*/
	public void SetFrag(Frag frag, int speed){
		this.frag=frag;
		this.speed=speed;
	}

	public void SetNoFrag(Vector3 target_no_frag, int speed){
		this.target_no_frag=target_no_frag;
		this.init_pos=transform.position;
		Vector3 targetDir = target_no_frag - transform.position;
        Vector3 forward = transform.forward;
        float angle = Vector3.Angle(targetDir, forward)*Mathf.Deg2Rad;
		this.speed=speed;
		this.speed_x=speed*Mathf.Sin(angle);
		this.speed_y=speed*Mathf.Cos(angle);
	}

	/*
		Used to refresh the split position when the screen resolution changes.
	*/
	public void RefreshPosition(){
		Update ();
	}

	public void InitMovement(){
		isStarted=true;
	}

	// Use this for initialization
	void Start () {
		time_passed=0;
	}
	
	// Update is called once per frame
	void Update () {
		time_passed+=Time.deltaTime;
		if (isStarted){
			if (this.frag!=null) {
				float step = speed * Time.deltaTime;
	        	transform.position = Vector3.MoveTowards(transform.position, frag.transform.position, step);
	        	// If it is out of the screen, destroy the frag
	        	if (transform.position.x<=-Screen.width/2-transform.localScale.x/2 || transform.position.z<=-Screen.height/2-transform.localScale.z/2){
	        		Destroy(gameObject);
	        	}
			} else {
				float step = speed * Time.deltaTime;
				// transform.position = Vector3.MoveTowards(transform.position, target_no_frag, step);
				transform.position=new Vector3(init_pos.x-speed_x*time_passed+0.5f*gravity*Mathf.Pow(time_passed,2), init_pos.y, init_pos.z+speed_y*time_passed);
	        	// If it is out of the screen, destroy the frag
	        	if (transform.position.x<=-Screen.width/2-transform.localScale.x/2 || transform.position.x>=Screen.width/2+transform.localScale.x/2 || transform.position.z<=-Screen.height/2-transform.localScale.z/2 || transform.position.z>=Screen.height/2+transform.localScale.z/2){
	        		Destroy(gameObject);
	        	}
			}
		}
	}

	void OnTriggerEnter(Collider collider) {
		if (this.frag!=null && collider.gameObject==this.frag.gameObject) {
        	Destroy(this.frag.gameObject);
        	Destroy(gameObject);
		}
    }
}
