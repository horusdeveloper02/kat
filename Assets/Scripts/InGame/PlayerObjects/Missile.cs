﻿using UnityEngine;
using System.Collections;

public class Missile : MonoBehaviour {

	private bool is_started=false;
	private int speed;
	public Vector3 init_pos;
	public Vector3 end_pos;

	public void RestartMovement (int speed) {
		is_started=true;
		transform.position=init_pos;
		this.speed=speed;
		renderer.enabled=true;
	}

	public bool HasReachedEnd () {
		return (transform.position==end_pos);
	}

	// Use this for initialization
	void Start () {
		// init_pos and end_pos are set on the Keep_Updated.Register function of MainGame
	}
	
	// Update is called once per frame
	void Update () {
		if (is_started){
			float step = speed * Time.deltaTime;
        	transform.position = Vector3.MoveTowards(transform.position, end_pos, step);
			if (HasReachedEnd()) {
				is_started=false;
				renderer.enabled=false;
			}
		}
	}
}
