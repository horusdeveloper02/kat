﻿using UnityEngine;
using System.Collections;

public class NumpadButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		 
	}

	void OnMouseUp () {
		if (Input.GetMouseButtonUp(0)) {
		 	GameObject text_obj=GameObject.Find("Numpad/"+name+"/Text");
		 	TextMesh text_obj_mesh=text_obj.GetComponent<TextMesh>();
		 	GameObject input_obj=GameObject.Find("Numpad/NumInput/Text");
		 	TextMesh input_obj_mesh=input_obj.GetComponent<TextMesh>();
		 	input_obj_mesh.text+=text_obj_mesh.text;
		 }
	}
}
