﻿using UnityEngine;
using System.Collections;

public class NumpadLaunch : MonoBehaviour {

		// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseUp () {
		if (Input.GetMouseButtonUp(0)) {
			TextMesh numpad_t_mesh=MainGame.numpad_input_text.GetComponent<TextMesh>();
		 	MainGame.LaunchMissiles(numpad_t_mesh.text);
		 }
	}
}
